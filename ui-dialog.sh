. './liblore.sh' --source-only

DIALOG_CANCEL=1
DIALOG_ESC=255
DIALOGRC=$PWD/.dialogrc

declare -ga registered_lists
declare -gA registered_lists_map
declare -ga mailing_list_patches

# Borrowed from kw
declare -gA configurations

function reset_arrays()
{
  registered_lists=()
  registered_lists_map=()
  mailing_list_patches=()
}

function parse_configuration()
{
  local config_path="$1"
  local value

  if [ ! -f "$config_path" ]; then
    return 22 # 22 means Invalid argument - EINVAL
  fi
  # shellcheck disable=SC2162
  while read line; do
    # Line started with # should be ignored
    [[ "$line" =~ ^# ]] && continue

    if printf '%s\n' "$line" | grep -F = &> /dev/null; then
      varname="$(printf '%s\n' "$line" | cut -d '=' -f 1 | tr -d '[:space:]')"
      value="$(printf '%s\n' "${line%#*}" | cut -d '=' -f 2-)"
      value="$(printf '%s\n' "$value" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')"

      configurations["$varname"]="$value"
    fi
  done < "$config_path"
}

function get_registered_mailing_lists()
{
  local count=1
  local index=0

  parse_configuration 'lore.config'

  # We need to convert the list to something that we can use in dialog
  IFS=',' read -r -a list_names <<< "${configurations['target_mailing_list']}"
  for list in "${list_names[@]}"; do
    registered_lists["$index"]="$count"
    ((index++))
    registered_lists["$index"]="$list"
    ((index++))
    registered_lists_map["$count"]="$list"
    ((count++))
  done
}

function show_patches_list()
{
  local list_values="$1"
  local tempfile='/tmp/test'

  dialog --checklist "Choose toppings:" 10 40 3 $list_values 2> $tempfile
  echo "$?"
}

function str_has_substring()
{
  local string="$1"
  local substring="$2"
}

function format_patch_row()
{
  local raw_data="$1"
  local patch_prefix
  local patch_version
  local total_patches
  local patch_title

  patch_prefix=$(printf '%s' "$raw_data" | grep -oP "^\[PATCH.*\]")
  if [[ "$?" != 0 ]]; then
    # Not a patch
    return
  fi

  # Patch version
  patch_version=$(printf '%s' "$patch_prefix" | grep -oP '[v|V]+\d+' | grep -oP '\d+')
  [[ "$?" != 0 ]] && patch_version=1

  # How many patches
  total_patches=$(printf '%s' "$patch_prefix" | grep -oP "\d+/\d+" | grep -oP "\d+$")
  [[ "$?" != 0 ]] && total_patches=1

  # Get patch title
  patch_title=$(printf '%s' "$raw_data" | cut -d ']' -f2)

  printf 'V%-2s |#%-3s| %-100s' "$patch_version" "$total_patches" "$patch_title"
}

function get_patch_info()
{
  local patch_index="$1"
  local target_patch

  target_patch=${list_of_mailinglist_patches["$patch_index"]}
  IFS="${SEPARATOR_CHAR}" read -r -a columns <<< "$target_patch"

  patch_metadata="\Zb\Z6Series:\Zn ${columns[4]}\n"
  patch_metadata+="\Zb\Z6Author:\Zn ${columns[0]}\n"
  patch_metadata+="\Zb\Z6Version:\Zn ${columns[2]}\n"
  patch_metadata+="\Zb\Z6Patches:\Zn ${columns[3]}\n"

  printf '%s' "$patch_metadata"
}

function ui_show_patch()
{
  local patch_index="$1"
  local patch_metadata
  local total
  local title
  local url

  patch_metadata=$(get_patch_info "$patch_index")

  while true; do
    exec 3>&1
    selection=$(dialog --backtitle 'kw Lore Interface' \
      --title 'Series info and actions' \
      --cancel-label 'Return' \
      --colors \
      --checklist "$patch_metadata" 20 100 2 \
      'Download mbox' '' off \
      'Bookmark' '' off \
      2>&1 1>&3)
      ret="$?"
      exec 3>&-

      # Default exit actions
      case "$ret" in
        "$DIALOG_CANCEL")
          clear
          return
          ;;
        "$DIALOG_ESC")
          clear
          return
          ;;
      esac

      for option in "${selection[@]}"; do
        if [[ "$option" =~ 'Download mbox' ]]; then
          local folder_name

          target_patch=${list_of_mailinglist_patches["$patch_index"]}
          IFS="${SEPARATOR_CHAR}" read -r -a columns <<< "$target_patch"
          total="${columns[3]},"
          title="${columns[4]}"
          url="${columns[5]}"

          folder_name=$(convert_title_to_folder_name "$title")
          download_series "$total" "$url" "$folder_name" "$title"
        fi
      done
  done
}

function ui_list_patches()
{
  local mailing_list="$1"
  declare -a display_patches_list

  get_patches_from_mailing_list "$mailing_list" display_patches_list

  while true; do
    exec 3>&1
    selection=$(dialog \
      --backtitle 'kw Lore Interface' --title "$mailing_list" \
      --clear \
      --cancel-label 'Return' \
      --menu 'Please select one of the patches:' 0 0 0 \
      "${display_patches_list[@]}" \
    2>&1 1>&3)
    ret="$?"
    exec 3>&-

    # Default exit actions
    case "$ret" in
      "$DIALOG_CANCEL")
        clear
        return
        ;;
      "$DIALOG_ESC")
        clear
        return
        ;;
    esac

    selection=$((selection - 1))
    ui_show_patch "$selection"
  done
}

function ui_lore_loop()
{
  local selection
  local index
  local target_mailing_list
  local ret

  reset_arrays

  # Let's start displaying the public mailing list registered by the user
  get_registered_mailing_lists

  # Core loop
  while true; do
    # We want to capture all 
    exec 3>&1
    selection=$(dialog --colors\
      --backtitle 'kw Lore Interface' --title 'Registered Mailing Lists' \
      --clear \
      --cancel-label 'Exit' \
      --menu 'Please select the target mailing list:' 0 80 0 \
      "${registered_lists[@]}" \
    2>&1 1>&3)
    ret="$?"
    exec 3>&-

    # Default exit actions
    case "$ret" in
      "$DIALOG_CANCEL")
        clear
        echo 'Program terminated.'
        exit
        ;;
      "$DIALOG_ESC")
        clear
        echo 'Program aborted.' >&2
        exit 1
        ;;
    esac

    index=$((selection))
    target_mailing_list="${registered_lists_map["$index"]}"
    # Now that we have the target, let's pull patches from lore
    ui_list_patches "$target_mailing_list"
  done
}

#show_patches_list "1 Cheese on 2 'Tomato' on 3 Anchovies off"
DIALOGRC=$PWD/.dialogrc ui_lore_loop
